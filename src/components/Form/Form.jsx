import React, {useCallback, useEffect, useState} from 'react';
import './Form.css';
import {useTelegram} from "../../hooks/useTelegram";


const Form = () => {
    const [copies, setCopies] = useState('');
    const [color, setColor] = useState('white-black');
    const {tg} = useTelegram();

    const onSendData = useCallback(() => {
        const data = {
            copies: copies,
            color: color,
            order_number: tg.InitData,
        }

        tg.sendData(JSON.stringify(data));
    }, [copies, color])

    useEffect(() => {
        tg.onEvent('mainButtonClicked', onSendData)
        return () => {
            tg.offEvent('mainButtonClicked', onSendData)
        }
    }, [onSendData])


    useEffect(() => {
        tg.MainButton.setParams({
            text: 'Получить чек на оплату',
        })
        tg.MainButton.show();
    }, [copies, color])
    const onChangeCopies = (e) => {
        setCopies(e.target.value)
    }

    const onChangeColor= (e) => {
        setColor(e.target.value)
    }

    return (
        <div className={"form"}>
            <h3>Введите ваши данные</h3>
            <input
                className={'input'}
                type="number" min={1} step={1}
                placeholder={'Количество копий'}
                value={copies}
                onChange={onChangeCopies}
            />

            <select value={color} onChange={onChangeColor} className={'select'}>
                <option value={'color'}> Цветная печать</option>
                <option value={'white-black'}> Черно-белая</option>
            </select>
        </div>
    );
};

export default Form;
